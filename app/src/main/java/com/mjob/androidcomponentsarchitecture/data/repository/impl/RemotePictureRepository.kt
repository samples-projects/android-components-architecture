package com.mjob.androidcomponentsarchitecture.data.repository.impl

import com.mjob.androidcomponentsarchitecture.data.api.PictureApiService
import com.mjob.androidcomponentsarchitecture.data.model.Picture
import com.mjob.androidcomponentsarchitecture.data.repository.PictureRepository
import retrofit2.Call
import javax.inject.Inject

class RemotePictureRepository @Inject constructor(
    private val pictureApiService: PictureApiService,
    private val pictureApiAccessToken: String
) :
    PictureRepository {
    override fun getPictures(page: Int, pageSize: Int): Call<List<Picture>> {
        return pictureApiService.getPictures("Client-ID $pictureApiAccessToken", page, pageSize)
    }
}