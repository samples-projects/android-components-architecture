package com.mjob.androidcomponentsarchitecture.data.api

import com.mjob.androidcomponentsarchitecture.data.model.Picture
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface PictureApiService {
    @GET("/photos")
    fun getPictures(
        @Header("Authorization") accessToken: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): Call<List<Picture>>
}