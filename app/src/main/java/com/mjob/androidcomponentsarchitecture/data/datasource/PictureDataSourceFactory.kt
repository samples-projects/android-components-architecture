package com.mjob.androidcomponentsarchitecture.data.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mjob.androidcomponentsarchitecture.data.model.Picture
import com.mjob.androidcomponentsarchitecture.data.repository.PictureRepository
import javax.inject.Inject

class PictureDataSourceFactory @Inject constructor(private val repository: PictureRepository) :
    DataSource.Factory<Int, Picture>() {
    val mutableSourceLiveData = MutableLiveData<PicturePageKeyedDataSource>()
    private var latestSource: PicturePageKeyedDataSource? = null
    override fun create(): DataSource<Int, Picture> {
        latestSource = PicturePageKeyedDataSource(repository)
        mutableSourceLiveData.postValue(latestSource)
        return latestSource as PicturePageKeyedDataSource
    }
}