package com.mjob.androidcomponentsarchitecture.data.repository

import com.mjob.androidcomponentsarchitecture.data.model.Picture
import retrofit2.Call

interface PictureRepository {
    fun getPictures(page: Int, pageSize: Int): Call<List<Picture>>
}