package com.mjob.androidcomponentsarchitecture.data.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.mjob.androidcomponentsarchitecture.data.model.NetworkState
import com.mjob.androidcomponentsarchitecture.data.model.Picture
import com.mjob.androidcomponentsarchitecture.data.repository.PictureRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class PicturePageKeyedDataSource @Inject constructor(
    private val pictureRepository: PictureRepository
) : PageKeyedDataSource<Int, Picture>() {
    var networkState: MutableLiveData<NetworkState> = MutableLiveData()

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Picture>) {
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Picture>) {
        networkState.postValue(NetworkState.LOADING)

        pictureRepository.getPictures(page = 1, pageSize = params.requestedLoadSize)
            .enqueue(object : Callback<List<Picture>> {
                override fun onResponse(call: Call<List<Picture>>, response: Response<List<Picture>>) {
                    if (response.isSuccessful) {
                        callback.onResult(response.body()!!, null, 2)
                        networkState.postValue(NetworkState.LOADED)
                    } else {
                        networkState.postValue(
                            NetworkState(
                                NetworkState.Status.FAILED,
                                response.message()
                            )
                        )
                    }
                }

                override fun onFailure(call: Call<List<Picture>>, t: Throwable) {
                    val errorMessage = t.message
                    networkState.postValue(
                        NetworkState(
                            NetworkState.Status.FAILED,
                            errorMessage
                        )
                    )
                }
            })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Picture>) {
        networkState.postValue(NetworkState.LOADING)

        pictureRepository.getPictures(page = params.key, pageSize = params.requestedLoadSize)
            .enqueue(object : Callback<List<Picture>> {
                override fun onResponse(call: Call<List<Picture>>, response: Response<List<Picture>>) {
                    if (response.isSuccessful) {
                        callback.onResult(response.body()!!, params.key + 1)
                        networkState.postValue(NetworkState.LOADED)
                    } else
                        networkState.postValue(
                            NetworkState(
                                NetworkState.Status.FAILED,
                                response.message()
                            )
                        )
                }

                override fun onFailure(call: Call<List<Picture>>, t: Throwable) {
                    val errorMessage = t.message
                    networkState.postValue(
                        NetworkState(
                            NetworkState.Status.FAILED,
                            errorMessage
                        )
                    )
                }
            })
    }
}