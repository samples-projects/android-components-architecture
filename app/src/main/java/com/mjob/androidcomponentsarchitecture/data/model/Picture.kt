package com.mjob.androidcomponentsarchitecture.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

data class Picture(
    @SerializedName("id")
    val id: String,
    @SerializedName("created_at")
    var postedAt: Date,
    @SerializedName("likes")
    val likes: Long,
    @SerializedName("urls")
    val pictureUrl: PictureUrl,
    @SerializedName("user")
    val author: Author
) : Serializable

data class Author(@SerializedName("username") val username: String)

data class PictureUrl(
    @SerializedName("full") val fullPictureUrl: String,
    @SerializedName("small") val smallPictureUrl: String
)