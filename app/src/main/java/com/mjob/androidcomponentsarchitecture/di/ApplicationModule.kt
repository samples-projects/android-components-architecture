package com.mjob.androidcomponentsarchitecture.di

import com.mjob.androidcomponentsarchitecture.BuildConfig
import com.mjob.androidcomponentsarchitecture.data.api.PictureApiService
import com.mjob.androidcomponentsarchitecture.data.datasource.PictureDataSourceFactory
import com.mjob.androidcomponentsarchitecture.data.repository.PictureRepository
import com.mjob.androidcomponentsarchitecture.data.repository.impl.RemotePictureRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
class ApplicationModule {
    @Provides
    fun provideRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .baseUrl(BuildConfig.PICTURE_API_BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun providePictureApiService(retrofit: Retrofit): PictureApiService {
        return retrofit.create(PictureApiService::class.java)
    }

    @Provides
    fun provideNetworkExecutor(): Executor {
        return Executors.newFixedThreadPool(THREAD_NUMBERS)
    }

    @Provides
    fun provideRemotePictureRepository(
        pictureApiService: PictureApiService
    ): PictureRepository {
        return RemotePictureRepository(pictureApiService, BuildConfig.PICTURE_API_ACCESS_KEY)
    }

    @Provides
    fun providePictureDataSourceFactory(
        repository: PictureRepository
    ): PictureDataSourceFactory {
        return PictureDataSourceFactory(repository)
    }

    companion object {
        const val THREAD_NUMBERS = 5
    }
}