package com.mjob.androidcomponentsarchitecture.di

import com.mjob.androidcomponentsarchitecture.ui.view.activity.MainActivity
import com.mjob.androidcomponentsarchitecture.ui.view.fragment.PictureDetailsFragment
import com.mjob.androidcomponentsarchitecture.ui.view.fragment.PictureListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewsModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity
    @ContributesAndroidInjector
    abstract fun contributesPictureListFragment(): PictureListFragment
    @ContributesAndroidInjector
    abstract fun contributesPictureDetailFragment(): PictureDetailsFragment
}