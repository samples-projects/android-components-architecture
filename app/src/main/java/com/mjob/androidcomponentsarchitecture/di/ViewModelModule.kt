package com.mjob.androidcomponentsarchitecture.di

import dagger.Module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.mjob.androidcomponentsarchitecture.di.factory.ViewModelFactory
import com.mjob.androidcomponentsarchitecture.viewmodel.PictureDetailsViewModel
import com.mjob.androidcomponentsarchitecture.viewmodel.PictureListViewModel
import dagger.Binds
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    /*
     * This method basically says
     * inject this object into a Map using the @IntoMap annotation,
     * with the  MovieListViewModel.class as key,
     * and a Provider that will build a MovieListViewModel
     * object.
     *
     * */

    @Binds
    @IntoMap
    @ViewModelKey(PictureListViewModel::class)
    internal abstract fun moviePictureViewModel(pictureListViewModel: PictureListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PictureDetailsViewModel::class)
    internal abstract fun moviePictureDetailsViewModel(pictureDetailsViewModel: PictureDetailsViewModel): ViewModel
}