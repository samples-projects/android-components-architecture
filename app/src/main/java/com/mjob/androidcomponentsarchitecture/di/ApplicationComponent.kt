package com.mjob.androidcomponentsarchitecture.di

import com.mjob.androidcomponentsarchitecture.PictureApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Component(
    modules = [AndroidSupportInjectionModule::class,
        ViewsModule::class,
        ApplicationModule::class,
        ViewModelModule::class]
)
interface ApplicationComponent : AndroidInjector<PictureApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<PictureApplication>()
}