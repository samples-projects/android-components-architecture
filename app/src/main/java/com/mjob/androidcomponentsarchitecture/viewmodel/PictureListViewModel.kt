package com.mjob.androidcomponentsarchitecture.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mjob.androidcomponentsarchitecture.data.datasource.PictureDataSourceFactory
import com.mjob.androidcomponentsarchitecture.data.model.NetworkState
import com.mjob.androidcomponentsarchitecture.data.model.Picture
import java.util.concurrent.Executor
import javax.inject.Inject

class PictureListViewModel @Inject constructor(
    var executor: Executor,
    var pictureDataSourceFactory: PictureDataSourceFactory
) : ViewModel() {
    lateinit var networkState: LiveData<NetworkState>
    lateinit var pictureLiveData: LiveData<PagedList<Picture>>

    init {
        setUp()
    }

    private fun setUp() {
        networkState =
            Transformations.switchMap(
                pictureDataSourceFactory.mutableSourceLiveData
            ) { dataSource -> dataSource.networkState }

        val pagedListConfig =
            PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(PAGE_SIZE)
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .build()

        pictureLiveData =
            LivePagedListBuilder(pictureDataSourceFactory, pagedListConfig)
                .setFetchExecutor(executor)
                .build()
    }

    companion object {
        const val PAGE_SIZE = 25
        const val PREFETCH_DISTANCE = 5
    }
}