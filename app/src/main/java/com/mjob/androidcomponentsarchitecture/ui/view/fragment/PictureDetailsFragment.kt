package com.mjob.androidcomponentsarchitecture.ui.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.mjob.androidcomponentsarchitecture.R
import com.mjob.androidcomponentsarchitecture.di.factory.ViewModelFactory
import com.mjob.androidcomponentsarchitecture.loadImageFromUrl
import com.mjob.androidcomponentsarchitecture.parentActivity
import com.mjob.androidcomponentsarchitecture.viewmodel.PictureDetailsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.picture_details_fragment.*
import javax.inject.Inject

class PictureDetailsFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: PictureDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.picture_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PictureDetailsViewModel::class.java)
        val toolbar = parentActivity().toolbar

        toolbar!!.title = "Full Picture"
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            Navigation.findNavController(view).popBackStack()
        }

        val picture = PictureDetailsFragmentArgs.fromBundle(arguments!!).picture
        fullPicture.loadImageFromUrl(picture.pictureUrl.fullPictureUrl, {
            image_loading_progressbar.visibility = View.INVISIBLE
        }, {
            Snackbar.make(view, R.string.error_open_file, Snackbar.LENGTH_LONG).show()
        })
    }
}
