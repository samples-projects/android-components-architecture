package com.mjob.androidcomponentsarchitecture.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.mjob.androidcomponentsarchitecture.R
import com.mjob.androidcomponentsarchitecture.data.model.Picture
import com.mjob.androidcomponentsarchitecture.ui.view.fragment.OnPictureItemClickListener
import javax.inject.Inject

class PictureListAdapter @Inject constructor() :
    PagedListAdapter<Picture, PictureViewHolder>(PICTURE_COMPARATOR) {
    var pictureItemClickListener: OnPictureItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_picture, parent, false)
        return PictureViewHolder(view)
    }

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        val picture = getItem(position)
        holder.bindTo(picture)
        holder.itemView.setOnClickListener {
            pictureItemClickListener?.openPicture(picture)
        }
    }

    companion object {
        private val PICTURE_COMPARATOR = object : DiffUtil.ItemCallback<Picture>() {
            override fun areItemsTheSame(oldPicture: Picture, newPicture: Picture): Boolean =
                oldPicture.id == newPicture.id

            override fun areContentsTheSame(oldPicture: Picture, newPicture: Picture): Boolean =
                oldPicture == newPicture
        }
    }
}
