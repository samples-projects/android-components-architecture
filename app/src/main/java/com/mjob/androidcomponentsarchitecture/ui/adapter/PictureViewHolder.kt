package com.mjob.androidcomponentsarchitecture.ui.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mjob.androidcomponentsarchitecture.R
import com.mjob.androidcomponentsarchitecture.data.model.Picture
import com.mjob.androidcomponentsarchitecture.dateToString
import com.mjob.androidcomponentsarchitecture.loadImageFromUrl

class PictureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindTo(picture: Picture?) {
        picture?.let {
            val pictureThumbnail: ImageView = itemView.findViewById(R.id.picture_thumbnail)
            pictureThumbnail.loadImageFromUrl(picture.pictureUrl.smallPictureUrl)

            val pictureAuthor: TextView = itemView.findViewById(R.id.picture_author_value)
            pictureAuthor.text = picture.author.username

            val pictureDescription: TextView = itemView.findViewById(R.id.picture_post_date_value)
            pictureDescription.text = picture.postedAt.dateToString()

            val pictureLikes: TextView = itemView.findViewById(R.id.picture_likes)
            pictureLikes.text = picture.likes.toString()
        }
    }
}