# android-components-architecture

Simple android project implementing some new components of Google.
In this sample we are displaying picture from UnSplash API

# Technology
This project is a picture gallery built with :
- [MVVM pattern](https://developer.android.com/jetpack/docs/guide)
- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)  for observing data and changing UI if it's case, in lifecycle aware approching
- [Paging](https://developer.android.com/topic/libraries/architecture/paging)  for paginating and loading new data when needed
- [Navigation](https://developer.android.com/guide/navigation)  for navigating more easier by only using fragments
- [Retrofit](https://square.github.io/retrofit/) for querying APIs easily
- [Glide](https://github.com/bumptech/glide) for image loading

Some other tools are used for project quality and commodity : 
- [Dagger2](https://github.com/google/dagger) for injecting dependencies
- [Ktlint](https://github.com/pinterest/ktlint) for code formatting following rules
- [Detekt](https://github.com/arturbosch/detekt) for static code analysis

# Architecture

```mermaid
graph LR
A((Fragment)) --> B((ViewModel))
B --> C((LiveData))
B --> D(DataSourceFactory)
D --> E(PagedKeyDataSource)
E --> F((Repository))
F --> G(API)
```

# Installation
- clone project repo
- register as developer to Unsplash
- Create an application for this project in your unsplash account
- copy that application access key
- open **gradle.properties** file create a variable named **picture_api_access_key** whose value will the access key surrounded with quotes

